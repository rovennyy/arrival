import requests

import pytest

from src.common import server_url
from src.datasets import bear_types, bear_names, bear_ages


class TestUpdateBear:

    @pytest.mark.parametrize("bear_type", bear_types)
    @pytest.mark.parametrize("bear_name", bear_names)
    @pytest.mark.parametrize("bear_age", bear_ages)
    def test_update_bear(self, create_bear, bear_type, bear_name, bear_age):
        """
        Проверка получится ли обновить существующую запись на те же параметры, кторые использовались в тестах создания записей
        """

        expected_code = 200

        response = requests.put(f"{server_url}/bear/{create_bear['bear_id']}", json=dict(bear_type=bear_type, bear_name=bear_name, bear_age=bear_age))
        assert response.status_code == expected_code, f"Вместо ожидаемого кода {expected_code} получено: {response.text}"
        assert response.text == "OK", f"Вместо 'OK' получено: {response.text}"

        expected_item = dict(bear_id=create_bear["bear_id"],
                             bear_type=bear_type,
                             bear_name=bear_name.upper(),
                             bear_age=float(bear_age))
        check_resp = requests.get(f"{server_url}/bear/{create_bear['bear_id']}")
        assert check_resp.json() == expected_item, f"На сервере сохранилось: {check_resp.text}"


