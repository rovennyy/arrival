import requests
import random

import pytest

from src.common import server_url


@pytest.fixture(scope="function")
def delete_one_item_after_test():
    """
    Нужна для того чтобы подчистить БД после теста на создание учетных записей
    """
    item_id = []
    yield item_id
    response = requests.delete(f"{server_url}/bear/{item_id[0]}")


@pytest.fixture(scope="function", params=[{"amount": 10}])
def bears_generator(request):
    """
    Генерирует список медведей, чтобы проверить функционал по получению списка
    """
    generated_items = []
    for i in range(request.param["amount"]):
        response = requests.post(f"{server_url}/bear", 
                                 json=dict(bear_type="BLACK", bear_name=f"generatedBear{i}", bear_age=10))
        generated_items.append(int(response.text))
    yield generated_items
    for i in generated_items:
        response = requests.delete(f"{server_url}/bear/{i}")


@pytest.fixture(scope="function", params=[{"bear_type": "BLACK", "bear_name": "Vasya", "bear_age": 10}])
def create_bear(request):
    """
    Создает одну заранее известную запись и возвращает ее ID для провеки функции получения и обновления записи
    """
    response = requests.post(f"{server_url}/bear", 
                                json=request.param)
    new_id = int(response.text)
    created_bear = request.param.copy()
    created_bear["bear_id"] = new_id
    yield created_bear
    response = requests.delete(f"{server_url}/bear/{new_id}")



@pytest.fixture(scope="class", autouse=True)
def cleaning():
    yield
    requests.delete(f"{server_url}/bear")
