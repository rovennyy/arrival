import requests

import pytest

from src.common import server_url
from src.datasets import bear_types, bear_names, bear_ages


class TestAddBear:

    @pytest.mark.parametrize("bear_type", bear_types)
    @pytest.mark.parametrize("bear_name", bear_names)
    @pytest.mark.parametrize("bear_age", bear_ages)
    def test_add_bear_full_request(self,
                                   delete_one_item_after_test,
                                   bear_type, bear_name, bear_age):
        """
        Проверка позитивных сценариев работы с интерфейсом. 
        Ожидается, что все варианты тестовых данных вернут успешный результат.

        Критерии: код ответа 200
        Содержимое ответа: ID новой записи (проверка по типу int)
        На сервисе должна быть сохранена запись с указанными при создании данными.
        """
        expected_code = 200

        response = requests.post(f"{server_url}/bear", json=dict(bear_type=bear_type, bear_name=bear_name, bear_age=bear_age))
        assert response.status_code == expected_code, f"Вместо ожидаемого кода {expected_code} получено: {response.text}"

        # возможно выглядит странно, это проверка, что в ответе возвращен ID записи
        # такие преобразования понадобились, потому что ответ возвращается в виде строки, которая может содержать как ID
        # в численном виде, так и строку с сообщением об ошибке
        # если возвращается строка, то в ассерте поднимается ошибка
        assert type(int(response.text)) == int, f"Вместо ID записи получено: {response.text}"

        # проверка того, что сохранилось на сервисе
        # с учетом того, что bear_name возвращается в верхнем регистре
        # а bear_age возвращается как float и имеет дробную часть

        new_id = int(response.text)

        # т.к. запись была успешно создана, в фикстуру нужно вернуть ее id, чтобы удалить ее после теста
        delete_one_item_after_test.append(new_id)

        expected_item = dict(bear_id=new_id,
                             bear_type=bear_type,
                             bear_name=bear_name.upper(),
                             bear_age=float(bear_age))
        check_resp = requests.get(f"{server_url}/bear/{new_id}")
        assert check_resp.json() == expected_item, f"На сервере сохранилось: {check_resp.text}"


# Самый тяжелый тест во всем задании)
# долго не мог решить сделать ли тесты заранее падающими, чтобы они требовали правильной реализации API
# или просто прочекать, что текущие варианты просто работают. Все таки выбрал второе.

# хорошим подходом было бы, в случае ошибки клиента (который отправил неправильные данные в запросе
# или не указал какой-либо параметр), обработать это на сервере и вернуть ответ с 400 кодом ошибки и сообщением,
# уточняющим причину.
# в предоставленом приложении обработка ошибок сделана не очень аккуратно:
# 1. если параметр был пропущен, то возвращается код 200 с сообщением о пропуске
# 2. если были указаны неправильные типы параметров, то приложение не обрабатывает ошибку и возвращает код 500
# В следующем тесте я планировал сделать проверки правильности обработки ошибок со стороны клиента, но
# из-за таких странностей с обработкой ошибок, ограничусь только проверками, что приложение возвращает
# сообщение о том, что параметры отсутствуют.
# Проверка невалидных значений остается в test_add_bear_full_request, в результате тестов вернуться xfail
#


    @pytest.mark.parametrize(
        "params, expected_code, expected_message",
        [
            ({"bear_type": "BLACK", "bear_name": "mikhail"},  200, "Error. Pls fill all parameters"),
            ({"bear_type": "BLACK", "bear_age": 10}, 200, "Error. Pls fill all parameters"),
            ({"bear_name": "mikhail", "bear_age": 100}, 200, "Error. Pls fill all parameters"),
        ]
    )
    def test_add_bear_incoplete_request(self, params, expected_code, expected_message):
        """
        Проверка негативных сценариев.
        Ожидается, что в случае различных ошибок в параметрах, будут вовзвращены соответствующие сообщения об ошибках.

        """
        response = requests.post(f"{server_url}/bear", json=params)
        assert response.status_code == expected_code, f"Вместо кода {expected_code} получено: {response.text}"
        assert response.text == expected_message, f"Вместо ожидаемого сообщения получено: {response.text}"
