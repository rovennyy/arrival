import requests

import pytest

from src.common import server_url


class TestDeleteBear:

    def test_delete_one_item(self, create_bear, bears_generator):
        """
        Проверка, что запись будет удалена, но остльные записи при этом останутся
        """
        response = requests.delete(f"{server_url}/bear/{create_bear['bear_id']}")
        assert response.status_code == 200, f"Вместо ожидаемого кода 200 получено: {response.text}"

        response = requests.get(f"{server_url}/bear/{create_bear['bear_id']}")
        assert response.text == 'EMPTY', f"Не удалось удалить запись, на сервер сохранилось: {response.text}"

        response = requests.get(f"{server_url}/bear")
        assert len(response.json()) > 0, f"Не удалось удалить запись, на сервер сохранилось: {response.text}"


    def test_delete_all_items(self, bears_generator):
        """
        Проверка, что все записи будут удалены
        """
        response = requests.delete(f"{server_url}/bear")
        assert response.status_code == 200, f"Вместо ожидаемого кода 200 получено: {response.text}"

        response = requests.get(f"{server_url}/bear")
        assert len(response.json()) == 0, f"Не удалось удалить запись, на сервер сохранилось: {response.text}"

