import requests

import pytest

from src.common import server_url


class TestRetrieveBears:

    def test_get_one_item(self, create_bear):
        """
        Тест проверяет, что сервис возвращает именно та запись, которая была создана
        """
        expected_bear = dict(bear_id=create_bear["bear_id"],
                             bear_type=create_bear["bear_type"],
                             bear_name=create_bear["bear_name"].upper(),
                             bear_age=float(create_bear["bear_age"]))
        response = requests.get(f"{server_url}/bear/{create_bear['bear_id']}")
        assert response.status_code == 200, response.text
        assert response.json() == expected_bear, response.text

    def test_get_all_items(self, bears_generator):
        """
        Тест проверяет, что сервис возвращает все созданные записи
        """

        response = requests.get(f"{server_url}/bear")
        assert response.status_code == 200,  f"Вместо ожидаемого кода 200 получено: {response.text}"
        assert len(response.json()) == len(bears_generator),  f"Ожидаемая длина списка {len(bears_generator)}, получено: {response.text}"
