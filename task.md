Представим, что тебе поступила задача протестировать REST API сервис, предоставляющий CRUD интерфейс для управления данными в нем.
Докер образ сервиса доступен для скачивания в репозитории: https://hub.docker.com/r/azshoo/alaska с тэгом образа 1.0
При запуске контейнера стартует приложение, доступное внутри контейнера по адресу http://0.0.0.0:8091
Все имеющееся описание по принципам работы сервиса можно получить, отправив GET запрос на эндпоинт /info

Необходимо:
- Скачать докер образ и запустить приложение.
- Составить список проверок, которые необходимо провести чтобы убедиться в работоспособности сервиса.
- Написать более пяти примеров автотестов на разные API методы, реализующих проверки из описанных в предыдущем пункте.

Условия:
- Мы никак не ограничиваем тебя в формате описания необходимых проверок (полнота и форма описания, чеклист vs тест-кейсы и пр.), главное - чтобы описание проверок было прозрачным и достаточным для понимания сути проверки.
- Для реализации автоматизированных тестов API нужно использовать PyTest.
- Результат надо опубликовать на Github или Gitlab.


Welcome to Alaska! This is CRUD service for bears in alaska. CRUD routes presented with REST naming notation: 
POST /bear - create 
GET /bear - read all bears 
GET /bear/:id - read specific bear 
PUT /bear/:id - update specific bear 
DELETE /bear - delete all bears 
DELETE /bear/:id - delete specific bear 
Example of ber json: {"bear_type":"BLACK","bear_name":"mikhail","bear_age":17.5}. Available types for bears are: POLAR, BROWN, BLACK and GUMMY.
