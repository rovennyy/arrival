import pytest


bear_types = [
        "POLAR",
        "BROWN",
        "BLACK",
        "GUMMY",
        pytest.param("OTHER_TYPE", marks=pytest.mark.xfail),
    ]

bear_names =[
        "a",
        "Petya",
        "123",
        "a" * 64,
        "Михаил",
        pytest.param("!@#$%^&*()_+=-098 7654321{}|\'\"'<>/?", marks=pytest.mark.xfail(reason="Спецсимволы не должны использоваться в имени медведя", run=True)),
        pytest.param("", marks=pytest.mark.xfail(reason="Имя медведя не должно быть пустым", run=True)),
        pytest.param(123, marks=pytest.mark.xfail(reason="Имя медведя не должно быть цифрами", run=True)),
    ]

bear_ages = [
        0.1,
        1,
        99,
        pytest.param(0, marks=pytest.mark.xfail(reason="возраст не должен быть равен 0", run=True)),
        pytest.param(-1, marks=pytest.mark.xfail(reason="возраст не должен быть меньше 0", run=True)),
        pytest.param(99, marks=pytest.mark.xfail(reason="слишком большой возраст для медведя)", run=True))
    ]
