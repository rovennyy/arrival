import os


server_ip = os.environ.get("ALASKA_ADDRESS", "127.0.0.1")
server_port = os.environ.get("ALASKA_PORT", "80")
server_proto = os.environ.get("ALASKA_PROTO", "http")

server_url = f"{server_proto}://{server_ip}:{server_port}"



