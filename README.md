# arrival

Для сборки url тестового сервера используются переменные окружения (здесь они равны дефолтным значениям, которые будут использоваться если переменные не указаны):

ALASKA_ADDRESS=127.0.0.1
ALASKA_PORT=80
ALASKA_PROTO=http


запуск тестов из корневой директории проекта
python -m pytest tests

установка необходимых модулей
python -m pip install -r requirements.txt
